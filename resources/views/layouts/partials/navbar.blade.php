<nav class="navbar navbar-header navbar-expand-lg fixed-top" style="background-color: white">
    <a class="container navbar-brand" href="/">
        <img src="{{asset('template/img/15SM.png')}}" height="40" alt="15SM Logo">
      </a>
    @guest
    <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
        <li class="nav-item u-text mx-2">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
        <li class="nav-item u-text mx-3">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
        @endif
        @else
        <div class="container-fluid">
            <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="/" id="home" role="button">
                        <i class="la la-home"></i>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="/post/create" id="plus-circle" role="button">
                        <i class="la la-plus-circle"></i>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false"> <span >{{ Auth::user()->name }}</span></span> </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <div class="user-box">
                                <div class="u-text">
                                    <h4>{{ Auth::user()->name }}</h4>
                                    <p class="text-muted">{{ Auth::user()->email }}</p><a href="/profile/{{ Auth::id() }}" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
                                </div>
                            </div>
                        </li>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">{{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                        </form>

                    </ul>
                </li>
            </ul>
        </div>
        @endguest
    </ul>
</nav>
