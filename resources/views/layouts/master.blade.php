<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>@yield('title')</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
	<link rel="stylesheet" href="{{ asset('template/css/ready.css') }}">
	<link rel="stylesheet" href="{{ asset('template/css/demo.css') }}">
	<script src="https://kit.fontawesome.com/6f68ec3c54.js" crossorigin="anonymous"></script>
    @stack('styles')
</head>

	<body style="background-color: #D3D3D3">

		@include('layouts.partials.navbar')

		<div class="container" style="margin-top:5rem; margin-bottom:5rem">
			<div class="container my-4 pt-4">
					@yield('content')
			</div>		
		</div>
		@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
	</body>

<script src="{{ asset('template/js/core/jquery.3.2.1.min.js') }}"></script>
<script src="{{ asset('template/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/js/core/popper.min.js') }}"></script>
<script src="{{ asset('template/js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/js/plugin/chartist/chartist.min.js') }}"></script>
<script src="{{ asset('template/js/plugin/chartist/plugin/chartist-plugin-tooltip.min.js') }}"></script>
<script src="{{ asset('template/js/plugin/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('template/js/plugin/jquery-mapael/maps/world_countries.min.js') }}"></script>
<script src="{{ asset('template/js/plugin/chart-circle/circles.min.js') }}"></script>
<script src="{{ asset('template/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ asset('template/js/ready.min.js') }}"></script>
<script src="{{ asset('template/js/demo.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@stack('scripts')
</html>