@extends('layouts.master')

@section('title')
Show Post
@endsection

@section('content')
<div class="card gedf-card">
    <div class="card-header">
        <div class="d-flex justify-content-between align-items-center">
            <div class="d-flex justify-content-between align-items-center">
                <div class="mr-2">
                    <img class="rounded-circle" width="45" src="https://picsum.photos/50/50" alt="">
                </div>
                <div class="ml-2">
                    <div class="h5 m-0">{{ $post->user->name }}</div>
                    <a href="/profile/{{ $post->user->id }}"
                        class="h7 text-muted">{{ $post->user->profile->username }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i> {{ $post->created_at->diffForHumans() }}</div>
        <div class="row">
            <div class="col-md-6">
                <img src="{{asset('img/post/' . $post->image)}}" class="card-img" >
            </div>
            <div class="col-md-6">
                <p class="card-text mt-1 text-center">
                    {!! $post->caption !!}
                    </p>
                <div class="d-flex flex-row bd-highlight">
                    <div class="p-2 bd-highlight">
                        @if (Auth::id()==$post->user->id)

                        @if($like==0)
                        <a href="/like/{{$post->id}}" class="btn btn-primary"><i class="fas fa-thumbs-up"></i> Like ({{ $jumlike }})</a>
                        @else
                        <a href="/like/{{$post->id}}" class="btn btn-primary disabled"><i class="fas fa-thumbs-up"></i>
                            Like ({{ $jumlike }})</a>
                        @endif
                        </form>
                        @else
                        @if($like==0)
                        <a href="/like/{{$post->id}}" class="btn btn-primary"><i class="fas fa-thumbs-up"></i> Like ({{ $jumlike }})</a>
                        @else
                        <a href="/like/{{$post->id}}" class="btn btn-primary disabled"><i class="fas fa-thumbs-up"></i>
                            Like ({{ $jumlike }})</a>
                        @endif
                        @endif
                    </div>
                    <div class="p-2 bd-highlight">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-success disabled"><i class="fas fa-comments"></i>  Comment ({{ $jumcomment }})</a></button>
                        </div>
                    </div>
                </div>
                

                <div class="comment-section border border-dark mt-3">
                    <div class="d-flex flex-column" style="max-height: 300px;overflow-y: auto;">
                        @php
                        $i = 0;
                        @endphp
                        @forelse ($comment as $item)
                        <div class="bg-white p-2">
                            <div class="d-flex flex-row user-info"><img class="rounded-circle"
                                    src="https://picsum.photos/50/50" width="40">
                                <div class="d-flex flex-column justify-content-start ml-2"><span
                                        class="d-block font-weight-bold name">{{ $item->users->name }}</span></div>
                            </div>
                            <div class="mt-2">
                                <p class="comment-text">{!!$item->comment!!}</p>
                            </div>
                        </div>
                        @foreach ($likecomment as $likes)
                        @if ($likes->comment_id == $item->id)
                        @php
                        $i = 1;
                        @endphp
                        @break
                        @else
                        @php
                        $i = 0;
                        @endphp
                        @endif
                        @endforeach
                        @if($i==1)
                        <div class="d-flex flex-row fs-12">
                            <a href="#" class="like p-2 cursor" aria-disabled="true"><i
                                    class="fa fa-thumbs-o-up"></i><span class="ml-1">Liked</span>
                            </a>
                        </div>
                        @else
                        <div class="d-flex flex-row fs-12">
                            <a href="/like/{{ $post->id }}/{{ $item->users->id }}/{{$item->id}}"
                                class="like p-2 cursor"><i class="fa fa-thumbs-o-up"></i><span class="ml-1">Like</span>
                            </a>
                        </div>
                        @endif
                        @empty
                        <div class="bg-white p-2">
                            <div class="d-flex flex-column justify-content-start ml-2">
                                <span class="d-block font-weight-bold name text-center">No Comment</span></div>
                        </div>
                        @endforelse
                    </div>
                </div>

                <div class="bg-light p-2 mt-3">
                    <form action="/comment" method="POST">
                        @csrf
                        <input type="hidden" value="{{ Auth::id() }}" name="user_id">
                        <input type="hidden" value="{{ $post->id }}" name="post_id">
                        <div class="d-flex flex-row align-items-start"><img class="rounded-circle mr-2"
                                src="https://picsum.photos/100/100" width="40">
                            <textarea class="form-control ml-2 shadow-none textarea" name="comment"></textarea>
                        </div>
                        <div class="mt-2 text-right">
                            <button class="btn btn-primary btn-sm shadow-none" type="submit">Post comment</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/ana8h6b97pmus0ohphmx400krofzy91kc8q6i2g0u2w8e6gt/tinymce/5/tinymce.min.js"
    referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
        toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
    });

</script>
@endpush
