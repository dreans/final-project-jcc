@extends('layouts.master')

@section('title')
Home
@endsection


@section('content')
@forelse ($post as $item)
<div class="container">
    <div class="card gedf-card">
        <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="ml-2">
                        <div class="h5 m-0">{{ $item->user->name }}</div>
                        <a href="/profile/{{ $item->user->id }}"
                            class="h7 text-muted">{{ $item->user->profile->username }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="text-muted h7 mb-2"> 
                <i class="fa fa-clock-o"></i> {{ $item->created_at->diffForHumans() }}
            </div>
            <img src="{{asset('img/post/' . $item->image)}}" alt="" class="mx-auto d-block"
                    style="max-height: 500px">
            <p class="card-text mt-2">
                {!! $item->caption !!}
            </p>
        </div>
        <div class="card-footer">
            <div class="d-flex">
                @php
                $i = 0;
                @endphp
                @foreach ($like as $countlike)
                @if ($countlike->post_id == $item->id && Auth::id()==$countlike->user_id)
                @php
                $i = 1;
                @endphp
                @break
                @endif
                @endforeach
                @if($i==1)
                <div class="px-1">
                    <a href="" class="btn btn-primary disabled"><i class="fas fa-thumbs-up"></i> Like</a>
                </div>
                @else
                <div class="px-1">
                    <a href="/like/{{$item->id}}" class="btn btn-primary"><i class="fas fa-thumbs-up"></i> Like</a>
                </div>
                @endif
                <div class="px-1">
                    <a href="/post/{{$item->id}}" class="btn btn-info"><i class="far fa-eye"></i> See Post</a>
                </div>
                @if (Auth::id()==$item->user->id)
                <div class="px-1">
                    <a href="/post/{{$item->id}}/edit" class="btn btn-warning"><i class="fas fa-pencil-alt"></i>
                        Edit</a>
                </div>
                <div class="px-1">
                    <form action="/post/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@empty
<div class="card">
    <div class="card-body">
        <h5 class="card-title">No Post</h5>
    </div>
</div>
@endforelse
</div>
@endsection
