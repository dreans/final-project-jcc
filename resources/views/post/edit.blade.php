@extends('layouts.master')

@section('title')
    Edit
@endsection


@section('content')
<div class="container" style="background-color: white">
    <form action="/post/{{$post->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <h3 class="mb-4">Edit Post</h3>
            <label for="image">Image</label>
            <input type="file" class="form-control" name="image" id="image">
            @error('image')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        </div>
        <div class="form-group">
            <label for="caption">Caption</label>
            <textarea class="form-control" name="caption" id="caption" rows="5" cols="30" placeholder="Enter Cast caption">{!! $post->caption !!}</textarea>
            @error('caption')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary  m-2">Edit</button>
    </form>
</div>
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/ana8h6b97pmus0ohphmx400krofzy91kc8q6i2g0u2w8e6gt/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });
  </script>
@endpush