@extends('layouts.master')

@section('title')
Profile | {{$user->profile->username }}
@endsection

@section('content')
<div class="container" style="background-color:white;">
    <div class="card mb-3">
        <div class="row no-gutters">
            <div class="col-md-4 my-2">
                @if (is_null($profile->picture))
                <img src="{{asset('img/profile/kosong.png')}}" alt="" class="mx-auto d-block"
                    style="max-height: 200px">
                @endif
                <img src="{{asset('img/profile/' . $profile->picture)}}" alt="" class="mx-auto d-block"
                    style="max-height: 200px">
            </div>
            <div class="col-md-6">
                <div class="card-body">
                    <h5 class="card-title my-3">{{$user->name }}</h5>
                    @if(Auth::id()==$id)
                    <div class="row py-2">
                        <div class="col-sm">
                            <h6><b>{{ $jumpost }}</b> Post</h6>
                        </div>
                        <div class="col-sm">
                            <h6><b>{{ $jumfollower }}</b> Followers</h6>
                        </div>
                        <div class="col-sm">
                            <h6><b>{{ $jumfollowing }}</b> Following</h6>
                        </div>
                    </div>
                    @endif
                </div>
                @if (Auth::id()!=$id)
                <div class="col-sm mb-4">
                    <form action="/follow" method="POST">
                        @csrf
                        <input type="hidden" name="target_id" value="{{ $id }}">
                        <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                        @if($target != 1)
                        <button type="submit" class="btn btn-outline-info px-5">Follow</button>
                        @else
                        <button type="button" class="btn btn-white px-5">Followed</button>
                        @endif
                    </form>
                </div>
                @else
                @endif
                <div class="container">
                    <h6>{{$user->profile->username }}</h6>
                    <p class="card-text mb-3">{{$user->profile->bio }}</p>
                </div>
            </div>
            @if(Auth::id()==$id)
            <div class="col-md-2 ">
                <a href="{{ Auth::id() }}/edit" type="button" class="btn btn-info pull-right m-4">Edit Profile</a>
            </div>
            @endif
        </div>
    </div>
</div>

<div class="container mt-4" style="background-color: white">
    <div class="row py-3">
        @forelse($user->posting as $items)
        <div class="col-md-4">
            <a href="/post/{{ $items->id }}" class="text-decoration-none text-dark">
                <div class="card">
                    <img src="{{asset('img/post/' . $items->image)}}" class="card-img-top"  alt="...">
                    <div class="card-body">
                        <p class="card-text text-decoration-none">{!!$items->caption!!}</p>
                    </div>
                </div>
            </a>
        </div>
        @empty
        <p class="pl-2">No Post</p>
        @endforelse
    </div>
</div>
@endsection
