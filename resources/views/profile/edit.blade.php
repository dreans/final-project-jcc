@extends('layouts.master')

@section('title')
    Edit Profile    
@endsection   

@section('content')
<div class="container" style="background-color:white;">
    <h4 class="pt-3">Edit Profile {{$user->name}}</h4>
    <form action="/profile/{{$profile->id}}" method="POST" class="pb-3" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{$user->name}}" id="name" placeholder="Masukkan Nama">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" value="{{$profile->username}}" id="username" placeholder="Masukkan Username">
            @error('username')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email" value="{{$user->email}}" id="email" placeholder="Masukkan Email">
            @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="picture">Profile Picture</label>
            <input type="file" class="form-control" id="picture" name="picture">
            @error('picture')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" name="bio" cols="30" id="bio" placeholder="Masukkan Bio">{{$profile->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection