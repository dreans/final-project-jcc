<?php



use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'PostController@index');

    Route::get('/profile/{user_id}', 'ProfileController@index');
    Route::get('/profile/{user_id}/edit', 'ProfileController@edit');
    Route::put('/profile/{profile_id}', 'ProfileController@update');

    Route::post('/comment', 'FollowController@storecomment');

    Route::post('/follow', 'FollowController@store');

    Route::post('/post', 'PostController@store');
    Route::get('/post/create', 'PostController@create');

    Route::get('/like/{post_id}', 'LikeController@show');
    Route::get('/like/{post_id}/{user_id}/{comment_id}', 'LikeController@showlikecomment');
    Route::get('/post/{post_id}', 'PostController@show');
    Route::put('/post/{post_id}', 'PostController@update');
    Route::delete('/post/{post_id}', 'PostController@destroy');
    Route::get('/post/{post_id}/edit', 'PostController@edit');
});
