<h1><b> Final Project </b></h1>
<h2> Kelompok 15 </h2>

<ul><h2> Anggota Kelompok </h2>
    <li>William Felix</li>
    <li>Andre Natanael</li>
</ul>


<ul><h2> Pembagian Tugas </h2>
    <h3>William Felix :</h3>
    <ol>
        <li>Pembuatan Model + Eloquent</li>
        <li>Pembuatan Controller</li>
        <li>Pembuatan View</li>
        <li>Pembuatan CRUD</li>
        <li>Pembuatan Eloquent Relationships</li>
        <li>Pembuatan Library/Packages</li>
    </ol>
    <h3>Andre Natanael :</h3>
    <ol>
        <li>Pembuatan template dasar</li>
        <li>Pemilihan template</li>
        <li>Pembuatan ERD</li>
        <li>Pembuatan Routing</li>
        <li>Pembuatan Migrations</li>
        <li>Pembuatan Laravel Auth + Middleware</li>
        <li>Pembuatan ERD</li>
    </ol>
</ul>


<h2> Tema Project </h2>
<p><b>Sosial Media App dengan Laravel</b><br></p>
<p> Terdapat beberapa fitur utama:<br></p>
<ol>
    <li>User harus terdaftar di web untuk menggunakan layanan sosmed</li>
    <li>User dapat membuat postingan berupa: tulisan, gambar dengan caption, quote</li>
    <li>User dapat membuat, mengedit, dan menghapus pada postingan milik sendiri</li>
    <li>Seorang User dapat mengikuti (follow) ke banyak User lainnya. Seorang User dapat diikuti oleh banyak User lainnya</li>
    <li>Satu postingan dapat disukai(like) oleh banyak User. Satu User dapat menyukai (like) banyak Postingan</li>
    <li>Satu postingan dapat memiliki banyak komentar. Satu komentar dapat dikomentari oleh banyak user</li>
    <li>Satu komentar dapat disukai oleh banyak User. Satu User dapat menyukai banyak komentar</li>
    <li>Seorang User dapat membuat dan mengubah profile nya sendiri</li>
    <li>Pada halaman menampilkan postingan terdapat konten posting, komentar, jumlah komentar, jumlah like</li>
    <li>Pada halaman profile User terdapat biodata, jumlah user pengikut(follower), jumlah user yang diikuti (following)</li>
</ol>
<h1> ERD <br><h1>
![erd](/uploads/870eb86014141fa12d05b85d11614d72/erd.png)

<h2> Link Video </h2>
<p>Link Demo Aplikasi : https://bit.ly/PresentasiKel15-JCC</p>
<p>Link Deploy : http://shielded-island-21219.herokuapp.com</p>

