<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    //
    protected $table = 'followers';
    protected $fillable = ['target_id', 'user_id'];
    public $timestamps = true;

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
