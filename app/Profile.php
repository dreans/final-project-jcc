<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = ['username', 'bio', 'picture', 'user_id'];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
