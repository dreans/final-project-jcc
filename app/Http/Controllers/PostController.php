<?php

namespace App\Http\Controllers;

use App\Like;
use App\Post;
use App\User;
use App\Comment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use RealRashid\SweetAlert\Facades\Alert;


class PostController extends Controller
{
    public function index()
    {
        $post = Post::latest()->get();
        $like = Like::latest()->get();
        return view('post.index', compact(['post', 'like']));
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'caption' => 'required',
            'image' => 'image|file|max:2048'
        ]);
        $imageprofile = $request->image;
        $new_image = time() . '-' . $imageprofile->getClientOriginalName();

        Post::create([
            'image' => $new_image,
            'caption' => $request->caption,
            'user_id' => Auth::id()
        ]);

        $imageprofile->move('img/post/', $new_image);

        Alert::success('Post', 'Post Berhasil Dibuat', 'OK');
        return redirect('/');
    }

    public function show($id)
    {
        $post = Post::find($id);
        $like = Like::where('post_id', $id)->where('user_id', Auth::id())->first();
        $comment = Comment::where('post_id', $id)->get();
        $likecomment = Like::where('post_id', $id)->whereNotNull('comment_id')->get();

        $jumlike = Like::where('post_id', $id)->whereNull('comment_id')->count();
        $jumcomment = Like::where('post_id', $id)->whereNotNull('comment_id')->count();

        if (is_null($like)) {
            $like = 0;
        } else {
            $like = 1;
        }
        return view('post.show', compact(['post', 'like', 'comment', 'likecomment', 'jumlike', 'jumcomment']));
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'caption' => 'required',
        ]);

        if ($request->image == null) {
            $request->image = "";
        }

        $post = post::find($id);
        $post->caption = $request->caption;
        $post->image = $request->file('image')->store('post-image');
        $post->update();
        Alert::success('Post', 'Post Berhasil Diupdate', 'OK');
        return Redirect::back();
    }

    public function destroy($id)
    {
        Like::where('post_id', $id)->delete();
        Comment::where('post_id', $id)->delete();
        $post = Post::find($id);
        $post->delete();
        Alert::success('Post', 'Post Berhasil Dihapus', 'OK');
        return redirect('/');
    }
}
