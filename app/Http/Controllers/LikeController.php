<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use RealRashid\SweetAlert\Facades\Alert;

class LikeController extends Controller
{
    public function show($request)
    {
        Like::create([
            'user_id' => Auth::id(),
            'post_id' => $request,
        ]);
        Alert::success('Like', 'Like Berhasil', 'OK');
        return Redirect::back();
    }
    public function showlikecomment($post_id, $user_id, $comment_id)
    {
        Like::create([
            'user_id' => $user_id,
            'post_id' => $post_id,
            'comment_id' => $comment_id,
        ]);
        Alert::success('Like', 'Like Berhasil', 'OK');
        return Redirect::back();
    }
}
