<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Profile;
use App\Follower;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use RealRashid\SweetAlert\Facades\Alert;


class ProfileController extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
        $profile = Profile::find($id);
        $follow = Follower::where('target_id', $id)->where('user_id', Auth::id())->first();
        if (is_null($follow)) {
            $target = 0;
        } else {
            $target = 1;
        }
        $jumpost = Post::where('user_id', Auth::id())->count();
        $jumfollower = Follower::where('target_id', Auth::id())->count();
        $jumfollowing = Follower::where('user_id', Auth::id())->count();
        return view('profile.index', compact(['id', 'user', 'profile', 'target', 'jumpost', 'jumfollower', 'jumfollowing']));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $profile = Profile::find($id);
        return view('profile.edit', compact(['profile', 'user']));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required',
            'email' => 'required',
            'picture' => 'image|file|max:2048'

        ]);

        if (!is_null($request->picture)) {
            $imageprofile = $request->picture;
            $new_image = time() . '-' . $imageprofile->getClientOriginalName();
            $profile = Profile::find($id);
            $profile->picture = $new_image;
            $profile->update();
            $imageprofile->move('img/profile/', $new_image);
        } else {
            $imageprofile = $request->picture;
            $new_image = time() . '-' . 'kosong.png';
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->update();
            $profile = Profile::find($id);
            $profile->picture = $new_image;
            $profile->username = $request->username;
            $profile->bio = $request->bio;
            $profile->update();
        }
        Alert::success('Profile', 'Profile Berhasil Diupdate', 'OK');
        return Redirect::back();
    }
}
