<?php

namespace App\Http\Controllers;

use App\Like;
use App\Comment;
use App\Follower;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class FollowController extends Controller
{
    public function store(Request $request)
    {
        Follower::create([
            'target_id' => $request->target_id,
            'user_id' => $request->user_id,
        ]);
        return Redirect::back();
    }
    public function storecomment(Request $request)
    {
        Comment::create([
            'comment' => $request->comment,
            'user_id' => $request->user_id,
            'post_id' => $request->post_id,
        ]);
        return Redirect::back();
    }
}
