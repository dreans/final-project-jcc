<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'likes';
    protected $fillable = ['user_id', 'post_id', 'comment_id'];
    public $timestamps = true;

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_id', 'id');
    }
    public function post()
    {
        return $this->belongsToMany('App\Post', 'post_id', 'id');
    }
    public function comment()
    {
        return $this->belongsToMany('App\Post', 'comment_id', 'id');
    }
}
